ripgen
======

Plugin for syntax highlighting and code diagnostics for
the ripgen (*.rip) declaration language.


requirements
------------

It uses a language server written in golang.
Therefore you need to have go 1.18+ installed.