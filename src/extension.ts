import * as path from 'path';
import { spawn } from 'child_process';
import { ExtensionContext, window, OutputChannel } from 'vscode';

import {
	LanguageClient,
	LanguageClientOptions,
	ServerOptions,
} from 'vscode-languageclient/node';

let client: LanguageClient;

async function installLanguageServer(context: ExtensionContext, log: OutputChannel) {
	return new Promise<void>((resolve, reject) => {


		let builder = spawn('go',
			['build', '-o', path.join('bin', 'ripgen-ls'), './cmd'],
			{
				cwd: context.extensionPath,
				detached: true,
			},
		);

		builder.stdout.on('data', (data) => {
			log.appendLine(`stdout: ${data}`);
		});

		builder.stderr.on('data', (data) => {
			log.appendLine(`stderr: ${data}`);
		});

		builder.on('close', (code) => {
			log.appendLine(`close ${code}`);
			if(code === 0) {
				resolve();
			} else {
				reject("failed!");
			}
		});
	})
}



export async function activate(context: ExtensionContext) {
	const log = window.createOutputChannel('ripgen')
	log.appendLine(context.extensionPath);

	await installLanguageServer(context, log)

	const serverBinary = context.asAbsolutePath(
		path.join('bin', 'ripgen-ls')
	);

	log.appendLine(serverBinary)

	const serverOptions: ServerOptions = {
		run: { command: serverBinary },
		debug: { command: serverBinary },
	};

	const clientOptions: LanguageClientOptions = {
		documentSelector: [{ scheme: 'file', language: 'ripgen' }],
	};

	client = new LanguageClient(
		'ripgenls',
		'ripgen (langserv)',
		serverOptions,
		clientOptions, true
	);

	log.appendLine("start")

	client.start();
}

export function deactivate(): Thenable<void> | undefined {
	if (!client) {
		return undefined;
	}
	return client.stop();
}
